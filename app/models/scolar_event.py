"""évènements scolaires dans la vie d'un étudiant(inscription, ...)
"""
from app import db
from app.models import SHORT_STR_LEN


class ScolarEvent(db.Model):
    """Evenement dans le parcours scolaire d'un étudiant"""

    __tablename__ = "scolar_events"
    id = db.Column(db.Integer, primary_key=True)
    event_id = db.synonym("id")
    etudid = db.Column(
        db.Integer,
        db.ForeignKey("identite.id", ondelete="CASCADE"),
    )
    event_date = db.Column(db.DateTime(timezone=True), server_default=db.func.now())
    formsemestre_id = db.Column(
        db.Integer,
        db.ForeignKey("notes_formsemestre.id", ondelete="SET NULL"),
    )
    ue_id = db.Column(
        db.Integer,
        db.ForeignKey("notes_ue.id", ondelete="SET NULL"),
    )
    # 'CREATION', 'INSCRIPTION', 'DEMISSION',
    # 'AUT_RED', 'EXCLUS', 'VALID_UE', 'VALID_SEM'
    # 'ECHEC_SEM'
    # 'UTIL_COMPENSATION'
    event_type = db.Column(db.String(SHORT_STR_LEN))
    # Semestre compensé par formsemestre_id:
    comp_formsemestre_id = db.Column(
        db.Integer,
        db.ForeignKey("notes_formsemestre.id"),
    )
    etud = db.relationship("Identite", lazy="select", backref="events", uselist=False)
    formsemestre = db.relationship(
        "FormSemestre", lazy="select", uselist=False, foreign_keys=[formsemestre_id]
    )

    def to_dict(self) -> dict:
        "as a dict"
        d = dict(self.__dict__)
        d.pop("_sa_instance_state", None)
        return d

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}({self.event_type}, {self.event_date.isoformat()}, {self.formsemestre})"
