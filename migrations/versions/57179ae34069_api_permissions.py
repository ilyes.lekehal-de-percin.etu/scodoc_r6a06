"""api_permissions

Revision ID: 57179ae34069
Revises: d718533466aa
Create Date: 2022-07-28 21:11:24.959406

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "57179ae34069"
down_revision = "d718533466aa"
branch_labels = None
depends_on = None


def upgrade():
    # Modification des permissions API
    # APIView 1<<40 = 1099511627776 => ScoView = 4
    # APIEditGroups 1<<41 = 2199023255552 => EtudChangeGroups = 1<<16 65536
    op.execute(
        """
        update role set permissions = permissions | 4 where (permissions & 1099511627776) <> 0;
        update role set permissions = permissions | 65536 where (permissions & 2199023255552) <> 0;
        """
    )

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###
