"""Tests unitaires : bulletins de notes

Utiliser comme: 
    pytest tests/unit/test_bulletin_bonus.py

"""
from app.but.bulletin_but_pdf import BulletinGeneratorStandardBUT


def test_nobonus():
    assert BulletinGeneratorStandardBUT.affichage_bonus_malus({}) == []


def test_bonus_sport_nul():
    assert BulletinGeneratorStandardBUT.affichage_bonus_malus({"bonus": 0}) == []


def test_malus_nul():
    assert BulletinGeneratorStandardBUT.affichage_bonus_malus({"malus": 0}) == []


def test_bonus_et_malus_nuls():
    assert (
        BulletinGeneratorStandardBUT.affichage_bonus_malus({"bonus": 0, "malus": 0})
        == []
    )


def test_vrai_malus():
    assert BulletinGeneratorStandardBUT.affichage_bonus_malus({"malus": 0.1}) == [
        "Malus: 0.1"
    ]


def test_bonus_sport_et_vrai_malus():
    assert BulletinGeneratorStandardBUT.affichage_bonus_malus(
        {"malus": 0.12, "bonus": 0.23}
    ) == [
        "Bonus: 0.23",
        "Malus: 0.12",
    ]


def test_bonus_sport_seul():
    assert BulletinGeneratorStandardBUT.affichage_bonus_malus({"bonus": 0.5}) == [
        "Bonus: 0.5"
    ]


def test_bonus_sport_nul_et_vrai_malus():
    assert BulletinGeneratorStandardBUT.affichage_bonus_malus(
        {"bonus": 0, "malus": 0.5}
    ) == ["Malus: 0.5"]


def test_bonus_sport_et_malus_nul():
    assert BulletinGeneratorStandardBUT.affichage_bonus_malus(
        {"bonus": 0.5, "malus": 0}
    ) == [
        "Bonus: 0.5",
    ]


def test_faux_malus():
    assert BulletinGeneratorStandardBUT.affichage_bonus_malus({"malus": -0.6}) == [
        "Bonus: 0.6"
    ]


def test_sport_nul_faux_malus():
    assert BulletinGeneratorStandardBUT.affichage_bonus_malus(
        {"bonus": 0, "malus": -0.6}
    ) == ["Bonus: 0.6"]


def test_bonus_sport_et_faux_malus():
    assert BulletinGeneratorStandardBUT.affichage_bonus_malus(
        {"bonus": 0.3, "malus": -0.6}
    ) == [
        "Bonus sport/culture: 0.3",
        "Bonus autres: 0.6",
    ]
