<?xml version="1.0" encoding="UTF-8"?>
<referentiel_competence specialite="INFO"
                        specialite_long="Informatique"
                        type="B.U.T." annexe="17"
                        type_structure="type2"
                        type_departement="secondaire"
                        version="2021-12-11 00:00:00"
>
    <competences>
                <competence nom_court="Réaliser"
                    numero="1"
                    libelle_long="Développer — c’est-à-dire concevoir, coder, tester et intégrer — une solution informatique pour un client. "
                    couleur="c1"
                    id="c565dab0bf0c01a118811e1786bb2ac9">
            <situations>
                                <situation>Élaborer une application informatique</situation>
                                <situation>Faire évoluer une application informatique</situation>
                                <situation>Maintenir en conditions opérationnelles une application informatique</situation>
                            </situations>
            <composantes_essentielles>
                                <composante>en respectant les besoins décrits par le client</composante>
                                <composante>en appliquant les principes algorithmiques</composante>
                                <composante>en veillant à la qualité du code et à sa documentation</composante>
                                <composante>en choisissant les ressources techniques appropriées</composante>
                            </composantes_essentielles>
            <niveaux>
                                <niveau ordre="1" libelle="Développer des applications informatiques simples " annee="BUT1">
                <acs>
                                        <ac code="AC11.01">Implémenter des conceptions simples</ac>
                                        <ac code="AC11.02">Élaborer des conceptions simples</ac>
                                        <ac code="AC11.03">Faire des essais et évaluer leurs résultats en regard des spécifications</ac>
                                        <ac code="AC11.04">Développer des interfaces utilisateurs</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="2" libelle="Partir des exigences et aller jusqu’à une application complète " annee="BUT2">
                <acs>
                                        <ac code="AC21.01">Élaborer et implémenter les spécifications fonctionnelles et non fonctionnelles à partir des exigences </ac>
                                        <ac code="AC21.02">Appliquer des principes d’accessibilité et d’ergonomie</ac>
                                        <ac code="AC21.03">Adopter de bonnes pratiques de conception et de programmation</ac>
                                        <ac code="AC21.04">Vérifier et valider la qualité de l’application par les tests</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="3" libelle="Adapter des applications sur un ensemble de supports (embarqué, web, mobile, IoT…) " annee="BUT3">
                <acs>
                                        <ac code="AC31.01">Choisir et implémenter les architectures adaptées</ac>
                                        <ac code="AC31.02">Faire évoluer une application existante</ac>
                                        <ac code="AC31.03">Intégrer des solutions dans un environnement de production</ac>
                                    </acs>
                </niveau>
                            </niveaux>
        </competence>
                <competence nom_court="Optimiser"
                    numero="2"
                    libelle_long="Proposer des applications informatiques optimisées en fonction de critères spécifiques : temps d’exécution, précision, consommation de ressources.."
                    couleur="c2"
                    id="384dc89c6cc6856ab3b49535d650a9d8">
            <situations>
                                <situation>Améliorer les performances des programmes dans des contextes contraints</situation>
                                <situation>Limiter l’impact environnemental d’une application informatique</situation>
                                <situation>Mettre en place des applications informatiques adaptées et efficaces</situation>
                            </situations>
            <composantes_essentielles>
                                <composante>en formalisant et modélisant des situations complexes</composante>
                                <composante>en recensant les algorithmes et les structures de données usuels </composante>
                                <composante>en s’appuyant sur des schémas de raisonnement</composante>
                                <composante>en justifiant les choix et validant les résultats </composante>
                            </composantes_essentielles>
            <niveaux>
                                <niveau ordre="1" libelle="Appréhender et construire des algorithmes " annee="BUT1">
                <acs>
                                        <ac code="AC12.01">Analyser un problème avec méthode (découpage en éléments algorithmiques simples, structure de données...) </ac>
                                        <ac code="AC12.02">Comparer des algorithmes pour des problèmes classiques (tris simples, recherche...)</ac>
                                        <ac code="AC12.03">Formaliser et mettre en œuvre des outils mathématiques pour l’informatique</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="2" libelle="Sélectionner les algorithmes adéquats pour répondre à un problème donné " annee="BUT2">
                <acs>
                                        <ac code="AC22.01">Choisir des structures de données complexes adaptées au problème</ac>
                                        <ac code="AC22.02">Utiliser des techniques algorithmiques adaptées pour des problèmes complexes (par ex. recherche opérationnelle, méthodes arborescentes, optimisation globale, intelligence artificielle...)</ac>
                                        <ac code="AC22.03">Comprendre les enjeux et moyens de sécurisation des données et du code</ac>
                                        <ac code="AC22.04">Évaluer l’impact environnemental et sociétal des solutions proposées</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="3" libelle="Analyser et optimiser des applications" annee="BUT3">
                <acs>
                                        <ac code="AC32.01">Anticiper les résultats de diverses métriques (temps d’exécution, occupation mémoire, montée en charge...)</ac>
                                        <ac code="AC32.02">Profiler, analyser et justifier le comportement d’un code existant</ac>
                                        <ac code="AC32.03">Choisir et utiliser des bibliothèques et méthodes dédiées au domaine d&#039;application (imagerie, immersion, intelligence artificielle, jeux vidéos, parallélisme, calcul formel...)</ac>
                                    </acs>
                </niveau>
                            </niveaux>
        </competence>
                <competence nom_court="Administrer"
                    numero="3"
                    libelle_long="Installer, configurer, mettre à disposition, maintenir en conditions opérationnelles des infrastructures, des services et des réseaux et optimiser le système informatique d’une organisation "
                    couleur="c3"
                    id="59052642c0556be77a77c600e3d47bfe">
            <situations>
                                <situation>Déployer une nouvelle architecture technique</situation>
                                <situation>Améliorer une infrastructure existante</situation>
                                <situation>Sécuriser les applications et les services</situation>
                            </situations>
            <composantes_essentielles>
                                <composante>en sécurisant le système d’information</composante>
                                <composante>en appliquant  les normes en vigueur et les bonnes pratiques architecturales et de sécurité</composante>
                                <composante>en offrant une qualité de service optimale</composante>
                                <composante>en assurant la continuité d&#039;activité</composante>
                            </composantes_essentielles>
            <niveaux>
                                <niveau ordre="1" libelle="Installer et configurer un poste de travail " annee="BUT1">
                <acs>
                                        <ac code="AC13.01">Identifier les différents composants (matériels et logiciels) d’un système numérique </ac>
                                        <ac code="AC13.02">Utiliser les fonctionnalités de base d’un système multitâches / multiutilisateurs </ac>
                                        <ac code="AC13.03">Installer et configurer un système d’exploitation et des outils de développement </ac>
                                        <ac code="AC13.04">Configurer un poste de travail dans un réseau d’entreprise </ac>
                                    </acs>
                </niveau>
                                <niveau ordre="2" libelle="Déployer des services dans une architecture réseau" annee="BUT2">
                <acs>
                                        <ac code="AC23.01">Concevoir et développer des applications communicantes </ac>
                                        <ac code="AC23.02">Utiliser des serveurs et des services réseaux virtualisés</ac>
                                        <ac code="AC23.03">Sécuriser les services et données d’un système</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="3" libelle="Faire évoluer et maintenir un système informatique communicant en conditions opérationnelles " annee="BUT3">
                <acs>
                                        <ac code="AC33.01">Créer des processus de traitement automatisé (solution de gestion de configuration et de parc, intégration et déploiement continu...) </ac>
                                        <ac code="AC33.02">Configurer un serveur et des services réseaux de manière avancée (virtualisation...)</ac>
                                        <ac code="AC33.03">Appliquer une politique de sécurité au niveau de l’infrastructure</ac>
                                        <ac code="AC33.04">Déployer et maintenir un réseau d’organisation en fonction de ses besoins </ac>
                                    </acs>
                </niveau>
                            </niveaux>
        </competence>
                <competence nom_court="Gérer"
                    numero="4"
                    libelle_long="Concevoir, gérer, administrer et exploiter les données de l’entreprise et mettre à disposition toutes les informations pour un bon pilotage de l’entreprise"
                    couleur="c4"
                    id="b1be9963258bc50533398ff0f42b1b91">
            <situations>
                                <situation>Lancer un nouveau projet</situation>
                                <situation>Sécuriser des données</situation>
                                <situation>Exploiter des données pour la prise de décisions </situation>
                            </situations>
            <composantes_essentielles>
                                <composante>en respectant les réglementations sur le respect de la vie privée et la protection des données personnelles</composante>
                                <composante>en respectant les enjeux économiques, sociétaux et écologiques de l’utilisation du stockage de données, ainsi que les différentes infrastructures (data centers, cloud, etc.)</composante>
                                <composante>en s’appuyant sur des bases mathématiques</composante>
                                <composante>en assurant la cohérence et la qualité</composante>
                            </composantes_essentielles>
            <niveaux>
                                <niveau ordre="1" libelle="Concevoir et mettre en place une base de données à partir d’un cahier des charges client " annee="BUT1">
                <acs>
                                        <ac code="AC14.01">Mettre à jour et interroger une base de données relationnelle (en requêtes directes ou à travers une application) </ac>
                                        <ac code="AC14.02">Visualiser des données</ac>
                                        <ac code="AC14.03">Concevoir une base de données relationnelle à partir d’un cahier des charges </ac>
                                    </acs>
                </niveau>
                                <niveau ordre="2" libelle="Optimiser une base de données, interagir avec une application et mettre en œuvre la sécurité " annee="BUT2">
                <acs>
                                        <ac code="AC24.01">Optimiser les modèles de données de l’entreprise</ac>
                                        <ac code="AC24.02">Assurer la sécurité des données (intégrité et confidentialité)</ac>
                                        <ac code="AC24.03">Organiser la restitution de données à travers la programmation et la visualisation </ac>
                                        <ac code="AC24.04">Manipuler des données hétérogènes </ac>
                                    </acs>
                </niveau>
                                <niveau ordre="3" libelle="Administrer une base de données, concevoir et réaliser des systèmes d’informations décisionnels " annee="BUT3">
                <acs>
                                        <ac code="AC34.01">Capturer et stocker des ensembles volumineux et complexes de données hétérogènes</ac>
                                        <ac code="AC34.02">Préparer et extraire les données pour l’exploitation</ac>
                                        <ac code="AC34.03">Appliquer des méthodes d’exploration et d’exploitation des données (apprentissage, informatique décisionnelle ou fouille de données) </ac>
                                        <ac code="AC34.04">Mettre en production et optimiser le système de gestion de données de l’entreprise </ac>
                                    </acs>
                </niveau>
                            </niveaux>
        </competence>
                <competence nom_court="Conduire"
                    numero="5"
                    libelle_long="Satisfaire les besoins des utilisateurs au regard de la chaîne de valeur du client, organiser et piloter un projet informatique avec des méthodes classiques ou agiles"
                    couleur="c5"
                    id="de170ed9d5b9db7c256f17a866278eea">
            <situations>
                                <situation>Lancer un nouveau projet</situation>
                                <situation>Piloter le maintien d’un projet en condition opérationnelle </situation>
                                <situation>Faire évoluer un système d’information</situation>
                            </situations>
            <composantes_essentielles>
                                <composante>en adoptant une démarche proactive, créative et critique</composante>
                                <composante>en respectant les règles juridiques et les normes en vigueur</composante>
                                <composante>en communiquant efficacement avec les différents acteurs d’un projet</composante>
                                <composante>en sensibilisant à une gestion éthique, responsable, durable et interculturelle</composante>
                            </composantes_essentielles>
            <niveaux>
                                <niveau ordre="1" libelle="Identifier les besoins métiers des clients et des utilisateurs " annee="BUT1">
                <acs>
                                        <ac code="AC15.01">Appréhender les besoins du client et de l&#039;utilisateur</ac>
                                        <ac code="AC15.02">Mettre en place les outils de gestion de projet</ac>
                                        <ac code="AC15.03">Identifier les acteurs et les différentes phases d’un cycle de développement </ac>
                                    </acs>
                </niveau>
                                <niveau ordre="2" libelle="Appliquer une démarche de suivi de projet en fonction des besoins métiers des clients et des utilisateurs " annee="BUT2">
                <acs>
                                        <ac code="AC25.01">Identifier les processus présents dans une organisation en vue d’améliorer les systèmes d’information </ac>
                                        <ac code="AC25.02">Formaliser les besoins du client et de l&#039;utilisateur</ac>
                                        <ac code="AC25.03">Identifier les critères de faisabilité d’un projet informatique</ac>
                                        <ac code="AC25.04">Définir et mettre en œuvre une démarche de suivi de projet </ac>
                                    </acs>
                </niveau>
                                <niveau ordre="3" libelle="Participer à la conception et à la mise en œuvre d’un projet système d’information " annee="BUT3">
                <acs>
                                        <ac code="AC35.01">Mesurer les impacts économiques, sociétaux et technologiques d’un projet informatique </ac>
                                        <ac code="AC35.02">Savoir intégrer un projet informatique dans le système d’information d’une organisation </ac>
                                        <ac code="AC35.03">Savoir adapter un système d’information </ac>
                                    </acs>
                </niveau>
                            </niveaux>
        </competence>
                <competence nom_court="Collaborer"
                    numero="6"
                    libelle_long="Acquérir, développer et exploiter les aptitudes nécessaires pour travailler efficacement dans une équipe informatique"
                    couleur="c6"
                    id="f4fcf7b2f78dbd8e00a3374a7a6243c4">
            <situations>
                                <situation>Lancer un nouveau projet</situation>
                                <situation>Organiser son travail en relation avec celui de son équipe</situation>
                                <situation>Élaborer, gérer et transmettre de l’information</situation>
                            </situations>
            <composantes_essentielles>
                                <composante>en inscrivant sa démarche au sein d’une équipe pluridisciplinaire</composante>
                                <composante>en accompagnant la mise en œuvre des évolutions informatiques </composante>
                                <composante>en veillant au respect des contraintes juridiques</composante>
                                <composante>en développant une communication efficace et collaborative</composante>
                            </composantes_essentielles>
            <niveaux>
                                <niveau ordre="1" libelle="Identifier ses aptitudes pour travailler dans une équipe " annee="BUT1">
                <acs>
                                        <ac code="AC16.01">Appréhender l’écosystème numérique</ac>
                                        <ac code="AC16.02">Découvrir les aptitudes requises selon les différents secteurs informatiques</ac>
                                        <ac code="AC16.03">Identifier les statuts, les fonctions et les rôles de chaque membre d’une équipe pluridisciplinaire </ac>
                                        <ac code="AC16.04">Acquérir les compétences interpersonnelles pour travailler en équipe </ac>
                                    </acs>
                </niveau>
                                <niveau ordre="2" libelle="Situer son rôle et ses missions au sein d’une équipe informatique " annee="BUT2">
                <acs>
                                        <ac code="AC26.01">Comprendre la diversité, la structure et la dimension de l’informatique dans une organisation (ESN, DSI,...) </ac>
                                        <ac code="AC26.02">Appliquer une démarche pour intégrer une équipe informatique au sein d’une organisation</ac>
                                        <ac code="AC26.03">Mobiliser les compétences interpersonnelles pour travailler dans une équipe informatique</ac>
                                        <ac code="AC26.04">Rendre compte de son activité professionnelle </ac>
                                    </acs>
                </niveau>
                                <niveau ordre="3" libelle="Manager une équipe informatique" annee="BUT3">
                <acs>
                                        <ac code="AC36.01">Organiser et partager une veille technologique et informationnelle </ac>
                                        <ac code="AC36.02">Identifier les enjeux de l’économie de l’innovation numérique</ac>
                                        <ac code="AC36.03">Guider la conduite du changement informatique au sein d’une organisation </ac>
                                        <ac code="AC36.04">Accompagner le management de projet informatique</ac>
                                    </acs>
                </niveau>
                            </niveaux>
        </competence>
            </competences>
    <parcours>
                <parcour numero="0" libelle="C : Administration, gestion et exploitation des données" code="C">
                        <annee ordre="1">
                                <competence niveau="1" id="c565dab0bf0c01a118811e1786bb2ac9"/>
                                <competence niveau="1" id="384dc89c6cc6856ab3b49535d650a9d8"/>
                                <competence niveau="1" id="59052642c0556be77a77c600e3d47bfe"/>
                                <competence niveau="1" id="b1be9963258bc50533398ff0f42b1b91"/>
                                <competence niveau="1" id="de170ed9d5b9db7c256f17a866278eea"/>
                                <competence niveau="1" id="f4fcf7b2f78dbd8e00a3374a7a6243c4"/>
                            </annee>
                        <annee ordre="2">
                                <competence niveau="2" id="c565dab0bf0c01a118811e1786bb2ac9"/>
                                <competence niveau="2" id="384dc89c6cc6856ab3b49535d650a9d8"/>
                                <competence niveau="2" id="59052642c0556be77a77c600e3d47bfe"/>
                                <competence niveau="2" id="b1be9963258bc50533398ff0f42b1b91"/>
                                <competence niveau="2" id="de170ed9d5b9db7c256f17a866278eea"/>
                                <competence niveau="2" id="f4fcf7b2f78dbd8e00a3374a7a6243c4"/>
                            </annee>
                        <annee ordre="3">
                                <competence niveau="3" id="b1be9963258bc50533398ff0f42b1b91"/>
                                <competence niveau="3" id="de170ed9d5b9db7c256f17a866278eea"/>
                                <competence niveau="3" id="f4fcf7b2f78dbd8e00a3374a7a6243c4"/>
                            </annee>
                    </parcour>
                <parcour numero="0" libelle="B : Déploiement d’applications communicantes et sécurisées" code="B">
                        <annee ordre="1">
                                <competence niveau="1" id="c565dab0bf0c01a118811e1786bb2ac9"/>
                                <competence niveau="1" id="384dc89c6cc6856ab3b49535d650a9d8"/>
                                <competence niveau="1" id="59052642c0556be77a77c600e3d47bfe"/>
                                <competence niveau="1" id="b1be9963258bc50533398ff0f42b1b91"/>
                                <competence niveau="1" id="de170ed9d5b9db7c256f17a866278eea"/>
                                <competence niveau="1" id="f4fcf7b2f78dbd8e00a3374a7a6243c4"/>
                            </annee>
                        <annee ordre="2">
                                <competence niveau="2" id="c565dab0bf0c01a118811e1786bb2ac9"/>
                                <competence niveau="2" id="384dc89c6cc6856ab3b49535d650a9d8"/>
                                <competence niveau="2" id="59052642c0556be77a77c600e3d47bfe"/>
                                <competence niveau="2" id="b1be9963258bc50533398ff0f42b1b91"/>
                                <competence niveau="2" id="de170ed9d5b9db7c256f17a866278eea"/>
                                <competence niveau="2" id="f4fcf7b2f78dbd8e00a3374a7a6243c4"/>
                            </annee>
                        <annee ordre="3">
                                <competence niveau="3" id="c565dab0bf0c01a118811e1786bb2ac9"/>
                                <competence niveau="3" id="59052642c0556be77a77c600e3d47bfe"/>
                                <competence niveau="3" id="f4fcf7b2f78dbd8e00a3374a7a6243c4"/>
                            </annee>
                    </parcour>
                <parcour numero="0" libelle="D : Intégration d’applications et management du système d’information" code="D">
                        <annee ordre="1">
                                <competence niveau="1" id="c565dab0bf0c01a118811e1786bb2ac9"/>
                                <competence niveau="1" id="384dc89c6cc6856ab3b49535d650a9d8"/>
                                <competence niveau="1" id="59052642c0556be77a77c600e3d47bfe"/>
                                <competence niveau="1" id="b1be9963258bc50533398ff0f42b1b91"/>
                                <competence niveau="1" id="de170ed9d5b9db7c256f17a866278eea"/>
                                <competence niveau="1" id="f4fcf7b2f78dbd8e00a3374a7a6243c4"/>
                            </annee>
                        <annee ordre="2">
                                <competence niveau="2" id="c565dab0bf0c01a118811e1786bb2ac9"/>
                                <competence niveau="2" id="384dc89c6cc6856ab3b49535d650a9d8"/>
                                <competence niveau="2" id="59052642c0556be77a77c600e3d47bfe"/>
                                <competence niveau="2" id="b1be9963258bc50533398ff0f42b1b91"/>
                                <competence niveau="2" id="de170ed9d5b9db7c256f17a866278eea"/>
                                <competence niveau="2" id="f4fcf7b2f78dbd8e00a3374a7a6243c4"/>
                            </annee>
                        <annee ordre="3">
                                <competence niveau="3" id="c565dab0bf0c01a118811e1786bb2ac9"/>
                                <competence niveau="3" id="de170ed9d5b9db7c256f17a866278eea"/>
                                <competence niveau="3" id="f4fcf7b2f78dbd8e00a3374a7a6243c4"/>
                            </annee>
                    </parcour>
                <parcour numero="0" libelle="A : Réalisation d’applications : conception, développement, validation" code="A">
                        <annee ordre="1">
                                <competence niveau="1" id="c565dab0bf0c01a118811e1786bb2ac9"/>
                                <competence niveau="1" id="384dc89c6cc6856ab3b49535d650a9d8"/>
                                <competence niveau="1" id="59052642c0556be77a77c600e3d47bfe"/>
                                <competence niveau="1" id="b1be9963258bc50533398ff0f42b1b91"/>
                                <competence niveau="1" id="de170ed9d5b9db7c256f17a866278eea"/>
                                <competence niveau="1" id="f4fcf7b2f78dbd8e00a3374a7a6243c4"/>
                            </annee>
                        <annee ordre="2">
                                <competence niveau="2" id="c565dab0bf0c01a118811e1786bb2ac9"/>
                                <competence niveau="2" id="384dc89c6cc6856ab3b49535d650a9d8"/>
                                <competence niveau="2" id="59052642c0556be77a77c600e3d47bfe"/>
                                <competence niveau="2" id="b1be9963258bc50533398ff0f42b1b91"/>
                                <competence niveau="2" id="de170ed9d5b9db7c256f17a866278eea"/>
                                <competence niveau="2" id="f4fcf7b2f78dbd8e00a3374a7a6243c4"/>
                            </annee>
                        <annee ordre="3">
                                <competence niveau="3" id="c565dab0bf0c01a118811e1786bb2ac9"/>
                                <competence niveau="3" id="384dc89c6cc6856ab3b49535d650a9d8"/>
                                <competence niveau="3" id="f4fcf7b2f78dbd8e00a3374a7a6243c4"/>
                            </annee>
                    </parcour>
            </parcours>
</referentiel_competence>
