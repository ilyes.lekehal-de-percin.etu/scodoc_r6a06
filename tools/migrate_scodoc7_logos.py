# -*- mode: python -*-
# -*- coding: utf-8 -*-

import glob
import os
import shutil

from app.models import Departement


def migrate_scodoc7_dept_logos(dept_name=""):
    if dept_name:
        depts = Departement.query.filter_by(acronym=dept_name)
    else:
        depts = Departement.query
    n_dir = 0
    n_moves = 0
    n_depts = 0
    purged_candidates = []  # directory that maybe purged at the end
    for dept in depts:
        logos_dir7 = f"/opt/scodoc-data/config/logos/logos_{dept.acronym}"
        logos_dir9 = f"/opt/scodoc-data/config/logos/logos_{dept.id}"
        if os.path.exists(logos_dir7):
            print(f"Migrating {dept.acronym} logos...")
            purged_candidates.append(logos_dir7)
            n_depts += 1
            if not os.path.exists(logos_dir9):
                # print(f"renaming {logos_dir7} to {logos_dir9}")
                shutil.move(logos_dir7, logos_dir9)
                n_dir += 1
            else:
                # print(f"merging {logos_dir7} with {logos_dir9}")
                for logo in glob.glob(f"{logos_dir7}/*"):
                    # print(f"\tmoving {logo}")
                    fn = os.path.split(logo)[1]
                    if not os.path.exists(os.path.sep.join([logos_dir9, fn])):
                        shutil.move(logo, logos_dir9)
                        n_moves += 1
    n_purged = 0
    for candidate in purged_candidates:
        if len(os.listdir(candidate)) == 0:
            os.rmdir(candidate)
            n_purged += 1
    print(f"{n_depts} department(s) scanned")
    if n_dir:
        print(f"{n_dir} directory(ies) moved")
    if n_moves:
        print(f"{n_moves} file(s) moved")
    if n_purged:
        print(f"{n_purged} scodoc7 logo dir(s) removed")
    if n_dir + n_moves + n_purged == 0:
        print("nothing done")
    # print(f"moved {n_moves}/{n} etuds")
