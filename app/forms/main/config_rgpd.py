# -*- mode: python -*-
# -*- coding: utf-8 -*-

##############################################################################
#
# ScoDoc
#
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   Emmanuel Viennet      emmanuel.viennet@viennet.net
#
##############################################################################

"""
Formulaire configuration RGPD
"""

from flask_wtf import FlaskForm
from wtforms import SubmitField
from wtforms.fields.simple import TextAreaField


class ConfigRGPDForm(FlaskForm):
    "Formulaire paramétrage RGPD"
    rgpd_coordonnees_dpo = TextAreaField(
        label="Optionnel: coordonnées du DPO",
        description="""Le délégué à la protection des données (DPO) est chargé de mettre en œuvre
        la conformité au règlement européen sur la protection des données (RGPD) au sein de l’organisme.
        Indiquer ici les coordonnées (format libre) qui seront affichées aux utilisateurs de ScoDoc.
        """,
        render_kw={"rows": 5, "cols": 72},
    )

    submit = SubmitField("Valider")
    cancel = SubmitField("Annuler", render_kw={"formnovalidate": True})
